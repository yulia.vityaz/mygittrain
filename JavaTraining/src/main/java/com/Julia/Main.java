package com.Julia;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello :)");
        byte flat = 2;
        int age = 25;
        float couple = 2.1f;
        double marriage = 1.112;
        System.out.print("Flat : ");
        System.out.println(flat);
        System.out.print("Age : ");
        System.out.println(age);
        System.out.print("Couple : ");
        System.out.println(couple);
        System.out.print("Marriage : ");
        System.out.println(marriage);
        System.out.println("-----------");
        System.out.println("Flat : " + flat);
        System.out.println("Age : " + age);
        System.out.println("Couple : " + couple);
        System.out.println("Marriage : " + marriage);
        System.out.println("-----------");
        System.out.println("Flat : " + flat + "\n" + "Age : " + age + "\n" + "Couple : " + couple + "\n" + "Marriage : " + marriage);
        System.out.println("-----------");
        byte tomatoes = 2;
        byte cabbage = 1;
        System.out.println(tomatoes + cabbage);
        System.out.println("-----------");
        byte salad = 2 + 1;
        System.out.println(salad);
        System.out.println("-----------");
        int salad2 = tomatoes + cabbage;
        System.out.println(salad2);
        System.out.println("-----------");
        byte cucumbers = 2;
        int lettuce = 1;
        float salad3 = cucumbers + lettuce;
        System.out.println(salad3);
        System.out.println("-----------");
        float cucumber = 2.67f;
        double lettuc = 1.678;
        double salad4 = (cucumber + lettuc) + 1;
        System.out.println(salad4);
        System.out.println("-----------");
        char name1 = 'J';
        char name2 = 'U';
        char name3 = 'L';
        char name4 = 'I';
        char name5 = 'A';
        System.out.print(name1);
        System.out.print(name2);
        System.out.print(name3);
        System.out.print(name4);
        System.out.println(name5);
        System.out.println("-----------");
        //Homework: Try all operators//
    }

}