package com.Julia.homework;

import org.jetbrains.annotations.NotNull;

public class Additional123 {
    public static void main(String[] args) {
        var x = 1;
        var y = 1;
        System.out.println(addTask(x, y));
        System.out.println(addTasks(x, y));
    }

    //   84*x + 3*y
    static int addTask(int x, int y) {
        var add1 = mul(84, x);
        var add2 = mul(3, y);
        return sum(add1, add2);
    }

    //    448/y + y*x - 4*x
    static int addTasks(int x, int y) {
        var add3 = div(448, y);
        var add4 = mul(y, x);
        var add5 = mul(4, x);
        return add3 + add4 - add5;
    }

    static @NotNull String addTaskLetter(char param, int x, int y) {
        if (param == 'p') {
            return String.valueOf(addTask(x, y));
        } else if (param == 's') {
            return String.valueOf(addTasks(x, y));
        } else {
            return "Please try again!";
        }
    }

    static int mul(int number, int multi) {
        return number * multi;
    }

    static int sum(int number1, int number2) {
        return number1 + number2;
    }

    static int div(int number3, int number4) {
        return number3 / number4;
    }


}
