package com.Julia.homework;

public class Lesson2 {
    public static void main(String[] args) {
        int cup1 = 200;
        int cup2 = 300;
        int result1 = sum(cup1, cup2);
        System.out.println(result1);
        System.out.println("-----------");
        int cup3 = 150;
        int cup4 = 250;
        int result2 = sum(cup3, cup4);
        System.out.println(result2);
        System.out.println("-----------");
        int nail1 = 1;
        int nail2 = 2;
        float result3 = sub(nail1, nail2);
        System.out.println(result3);
        System.out.println("-----------");
        int nail3 = 3;
        int nail4 = 4;
        double result4 = mul(nail3, nail4);
        System.out.println(result4);
        System.out.println("-----------");
        int banana1 = 2;
        int banana2 = 3;
        double result5 = myUniqueCalculation(banana1, banana2);
        System.out.println(result5);
        System.out.println("-----------");


    }

    public static int sum(int a, int b) {
        return a + b;
    }

    public static float sub(int a, int b) {
        return a - b;
    }

    public static double mul(int a, int b) {
        return a * b;
    }

    public static double sum(float a, double b) {
        return a + b;
    }

    public static double divD(double a, int b) {
        return a / b;
    }


    public static long divL(long a, int b) {
        return a / b;
    }
    // формула (x+y) до квадрату//
    // функція + тест//
    // формула (x+y) 2   це число поділити на 3//
    //функція тест//
    //різні параметри//
//long, int, float, double = 32//

    public static double myUniqueCalculation(int x, int y) {
        int resultSum = sum(x, y);
        return Math.pow(resultSum, 2);
    }

    public static double myUniqueDivision(int x, int y) {
        double resultPow = myUniqueCalculation(x, y);
        return divD(resultPow, 3);
    }

    public static double myUniqueCalculationF(float x, double y) {
        double resultSumF = sum(x, y);
        return Math.pow(resultSumF, 2);
    }

    public static double myUniqueDivisionF(float x, double y) {
        double resultPowF = myUniqueCalculationF(x, y);
        return divD(resultPowF, 3);
    }

    public static long myUniqueCalculationL(int x, int y) {
        long resultSumL = sum(x, y);
        return (long) Math.pow(resultSumL, 2);
    }

    public static long myUniqueDivisionL(int x, int y) {
        long resultPowL = myUniqueCalculationL(x, y);
        return divL(resultPowL, 3);
    }

    public static double myUniqueCalculationTwo(int x, int y) {
        int resultSumTwo = sum(x, y);
        double resultPowTwo = Math.pow(resultSumTwo, 2);
        return divD(resultPowTwo, 3);
    }

    public static long myUniqueCalculationLTwo(int x, int y) {
        long resultSumLTwo = sum(x, y);
        long resultPowSumLTwo = (long) Math.pow(resultSumLTwo, 2);
        return divL(resultPowSumLTwo, 3);
    }

    public static double mySquare(int x, int y) {
        double resultSumSquare = sum(x, y);
        return Math.sqrt(resultSumSquare);
    }

    public static double myMin(double x, double y) {
        return Math.min(x, y);
    }

    public static double myMax(double x, double y) {
        return Math.max(x, y);
    }

    public static double myRandom() {
        return Math.random();
    }
}

