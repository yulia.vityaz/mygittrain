package com.Julia.homework;

public class Lesson3 {
    public static void main(String[] args) {
// x,y,v
//        1. x*3 + x/(x+4,5) == v
//        2. (3+y)^2/y == v
//        3. true=Winner = 1 and && 2 are true, false=looser
        var mathTaskA=mathTask1(3, 5);
        var mathTaskB=mathTask2(2, 5);
        if (mathTaskA&&mathTaskB) {
            System.out.println("Winner");
        } else {
            System.out.println("Looser");
        }
    }

    static boolean mathTask1(int x, int v) { //        1. x*3 + x/(x+4,5) == v
        var mulUp = mul(x, 3);
        var sumD = sumDown(x);
        var divD = div(x, sumD);
        var sumAll = mulUp + divD;
        return sumAll == v;
    }

    static boolean mathTask2(int y, int v) {//        2. (3+y)^2/y == v
        var sumY = sum2(y);
        var mulY = mul(2, sumY);
        var divY = div(mulY, y);
        return divY == v;
    }

    static double sumDown(int number) {
        return number + 4.5;
    }

    static int sum2(int y) {
        return 3 + y;
    }

    static double div(int number, double divider) {
        return number / divider;
    }


    void myFun2() {//       x^6 - x/10 >= y^3 + 3y;
        var result = numberFunction(2, 1);
        boolean p = true;
        boolean condition = result == p;
        if (condition) {
            System.out.println("Bravo!");
        } else {
            System.out.println("Looser =(");
        }
//        result >= p;


    }

    static boolean numberFunction(int x, int y) {
        var pow1 = pow(x, 6);
        var div1 = div(x, 10);
        var pow2 = pow(y, 3);
        var mul2 = mul(3, y);
        return pow1 - div1 >= pow2 + mul2;
    }

    static int pow(int number, int range) {
        return (int) Math.pow(number, range);
    }

    static int div(int number, int divider) {
        return number / divider;
    }

    static int mul(int multi, int number) {
        return number * multi;
    }


    // X+4 <= y+7
    // x != 5
//        System.out.println(notThatNumber(5));
//    static boolean notThatNumber(int x) {
//        return x!=5;
//    }
//        var x = 12;
//        var y = 3;
//        var result = checkCondition(x, y);
//        System.out.println(result);
//    }
//
//    static int sumX(int x) {
//        return x + 4;
//    }
//
//    static int sumY(int y) {
//        return y + 7;
//    }
//
//    static boolean checkCondition(int x, int y) {
//        var result1 = sumX(x);
//        var result2 = sumY(y);
//        return result1 <= result2;


    static void myFun() {
        boolean angry = false;
        if (angry) {
            System.out.println(1234);
        } else {
            System.out.println(5678);
        }
        boolean happy = false;
        if (happy) {
            System.out.println(91011);
        } else if (angry) {
            System.out.println(121314);
        } else {
            System.out.println("notNumber");
        }
        switch ("age") {
            case "name":
                System.out.println("Tania");
                break;
            case "age":
                System.out.println(22);
                break;
            default:
                System.out.println("do not understand");
        }
    }
}






