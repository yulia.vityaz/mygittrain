package com.Julia.homework;

public class Lesson4 {
    public static void main(String[] args) {
        // for(int i:){ //для масивів
        // new loops
        // Christmas tree?
    }

    public static void myFunFor() {
        for (int i = 0; i < 5; i++) {
            if (i == 3) {
                break;
            }
            System.out.println(i);
        }
        System.out.println("-----------");
        for (int d = 10; d > -1; d--) {
            if (d == 3) {
                break;
            }
            System.out.println(d);
        }
        System.out.println("-----------");
    }

    public static void myFunWhile() {
        int i = 5;
        int a = 1;
        while (i == 5) {
            a++;
            if (a == i) {
                break;
            }
            System.out.println(a);
        }
        int d = 0;
        int s = 60;
        while (d == 0) {
            s--;
            if (s == d) {
                break;
            }
            System.out.println(s);
        }
        int e = 5;
        int y = 1;
        while (e == 5) {
            y++;
            if (y == e) {
                e = 6;
            }
            System.out.println(a);
        }
    }

    static void myFunDoWhile() {
        int i = 0;
        do {
            i++;
            System.out.println(i);
        } while (i == 1);
        int s = 10;
        do {
            s--;
            System.out.println(s);
        } while (s >= 1);
    }
    static void myFunWhileTrue(){
        int i = 0;
        for (; ; ) {
            i++;
            if (i == 10) {
                break;
            }
            System.out.println(i);
        }
    }
}
