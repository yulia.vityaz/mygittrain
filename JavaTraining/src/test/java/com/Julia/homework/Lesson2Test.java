package com.Julia.homework;

import org.junit.Assert;
import org.junit.Test;


public class Lesson2Test {

    @Test
    public void testSum() {
        var sumResult = Lesson2.sum(4, 2);
        var expectedResult = 4 + 2;
        Assert.assertEquals(expectedResult, sumResult);
    }

    @Test
    public void testSub() {
        var subResult = Lesson2.sub(10, 5);
        var expectedResult = (10.0f - 5);
        Assert.assertEquals(expectedResult, subResult, 0);
    }

    @Test
    public void testMul() {
        var mulResult = Lesson2.mul(3, 4);
        var expectedResult = 3 * 4;
        Assert.assertEquals(expectedResult, mulResult, 0);
    }

    @Test
    public void testSumDoubleAndFloat() {
        var sumResult = Lesson2.sum(2.22f, 2.11);
        var expectedResult = 2.22f + 2.11;
        Assert.assertEquals(expectedResult, sumResult, 0);
    }

    @Test
    public void testMyUniqueCalculation() {
        var actualResult = Lesson2.myUniqueCalculation(2, 3);
        var expectedResult = 25;
        Assert.assertEquals(expectedResult, actualResult, 0);
    }

    @Test
    public void testMyUniqueDivision() {
        var divResult = Lesson2.myUniqueDivision(2, 3);
        var divResultS = 8.333333333333334;
        Assert.assertEquals(divResult, divResultS, 0);
    }

    @Test
    public void testMyUniqueCalculationF() {
        var sumResultF = Lesson2.myUniqueCalculationF(3f, 3);
        var sumResultsF = 36;
        Assert.assertEquals(sumResultF, sumResultsF, 0);
    }

    @Test
    public void testMyUniqueDivisionF() {
        var divResultF = Lesson2.myUniqueDivisionF(3f, 3);
        var divResultsF = 12;
        Assert.assertEquals(divResultF, divResultsF, 0);
    }

    @Test
    public void testMyUniqueCalculationL() {
        var sumResultL = Lesson2.myUniqueCalculationL(5, 10);
        var sumResultsL = 225;
        Assert.assertEquals(sumResultL, sumResultsL);
    }

    @Test
    public void testMyUniqueDivisionL() {
        var divResultL = Lesson2.myUniqueDivisionL(5, 10);
        var divResultsL = 75;
        Assert.assertEquals(divResultL, divResultsL);
    }

    @Test
    public void testMyUniqueCalculationTwo() {
        var sumDivResultTwo = Lesson2.myUniqueCalculationTwo(2, 4);
        var sumDivResultsTwo = 12;
        Assert.assertEquals(sumDivResultTwo, sumDivResultsTwo, 0);
    }

    @Test
    public void testMyUniqueCalculationLTwo() {
        var sumDivResultLTwo = Lesson2.myUniqueCalculationLTwo(7, 8);
        var sumDivResultsLTwo = 75;
        Assert.assertEquals(sumDivResultLTwo, sumDivResultsLTwo);
    }

    @Test
    public void testMySquare() {
        var squareResult = Lesson2.mySquare(4, 5);
        var squareResults = 3;
        Assert.assertEquals(squareResult, squareResults, 0);
    }

    @Test
    public void testMyMin() {
        var minResult = Lesson2.myMin(46.50, 48.20);
        var minResults = 46.50;
        Assert.assertEquals(minResult, minResults, 0);
    }

    @Test
    public void testMyMax() {
        var maxResult = Lesson2.myMax(50.80, 52.10);
        var maxResults = 52.10;
        Assert.assertEquals(maxResult, maxResults, 0);
    }

    @Test
    public void testMyRandom() {
        var randomResult = Lesson2.myRandom();
        Assert.assertTrue((0 <= randomResult) && (randomResult <= 1));
    }

}
