package com.Julia.homework;

import org.junit.Assert;
import org.junit.Test;

public class Lesson3Test {

    @Test
    public void testAddTask(){
        var x=1;
        var y=1;
        var result=Additional123.addTask(x,y);
        Assert.assertEquals(87,result);
    }

    @Test
    public void testAddTasks(){
        var x=1;
        var y=1;
        var result=Additional123.addTasks(x,y);
        Assert.assertEquals(445,result);
    }

    @Test
    public void testAddTaskLetterP(){
        var x=1;
        var y=1;
        char param='p';
        var result=Additional123.addTaskLetter(param,x,y);
        Assert.assertEquals("87",result);
    }

    @Test
    public void testAddTaskLetterS(){
        var x=1;
        var y=1;
        char param='s';
        var result=Additional123.addTaskLetter(param,x,y);
        Assert.assertEquals("445",result);
    }

    @Test
    public void testAddTaskLetterEmpty(){
        var x=1;
        var y=1;
        char param=' ';
        var result=Additional123.addTaskLetter(param,x,y);
        Assert.assertEquals("Please try again!",result);
    }
}
