# MyGitTrain

Aim - For training

Rules for organisation:
- firstly do merge request and assign it to the reviewer
- do not destroy the project ;)

Rules for develop branch:
- develop branch is protected 

How to:
 - How to create a new branch?
    * git branch new-branch
    or
    * git checkout -b new-branch 
    Important: name of a branch in a lowercase
 - How to push some changes?
    * git add . or git add new-file
    * git commit -m "a message about changes"
    * git push 
    Important: a message should be informative but not big, remember your login and password
 - How to create a merge request?
    * Push changes (you already know how to do that ;)
    * Click "Merge requests", than "new merge request"
    Important: do not forget to assign it to the reviewer, do merge request into develop branch